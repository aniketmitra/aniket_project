from gtts import gTTS
import speech_recognition as sr
import os
import time
import webbrowser
import pyautogui
from pynput.keyboard import Key ,Controller
keyboard=Controller()
def talkToMe(audio):
    print(audio)
    tts=gTTS(text=audio,lang='en')
    tts.save('audio.mp3')
    os.system('audio.mp3')
def myCommand():
    r=sr.Recognizer()
    with sr.Microphone() as source:
        print('I am ready for your command')
        r.pause_threshold=1
        r.adjust_for_ambient_noise(source,duration=1)
        audio=r.listen(source)
    try:
        command=r.recognize_google(audio)
        print('You said:'+command)
        talkToMe(command)
    except sr.UnknownValueError:
        assistant(myCommand())
    return command
def assistant(command):
    if 'open Google Chrome in Python'in command:
        time.sleep(2.5)
        talkToMe('What to search?')
        content=myCommand()
        time.sleep(3.5)
        print(content)
        talkToMe('You said:'+str(content)+'/n')
        if content==('Amazon'+str(content[6:])):
                webbrowser.open('https://www.amazon.in/s/ref=nb_sb_noss/258-0320375-3996313?url=search-alias%3Daps&field-keywords='+str(content))
        elif content==('Snapdeal'+str(content[8:])):
                webbrowser.open('https://www.snapdeal.com/search?keyword='+str(content))
        elif content==('Gmail'):
                webbrowser.open('https://accounts.google.com/ServiceLogin/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=AddSession')
                time.sleep(52)
                talkToMe('do you want to search something')
                content2=myCommand()
                #time.sleep(8)
                pyautogui.click(232,95)
                time.sleep(5)
                for char in content2:
                    keyboard.press(char)
                    keyboard.release(char)
                    #time.sleep(0.53)
                    keyboard.press(Key.enter)
                    keyboard.release(Key.enter)
        elif content==('Yahoo Mail'):
                webbrowser.open('https://login.yahoo.com/?.src=ym&.intl=in&.lang=en-IN&.partner=none&.done=https%3A%2F%2Fmail.yahoo.com%2F%3F.intl%3Din%26amp%3B.lang%3Den-IN%26amp%3B.partner%3Dnone%26amp%3B.src%3Dfp')
        elif content==('YouTube search'+str(content[14:])):
                webbrowser.open('https://www.youtube.com/results?search_query='+str(content[14:]))
        elif content==('check weather'+str(content[13:])):
                webbrowser.open('https://www.google.co.in/search?ei=NZrMWoj2O5eevQSrv7q4Cg&q=weather+check'+str(content[13:]))
        elif content==('play video directly'):
                time.sleep(2.5)
                talkToMe('Which song you want me to play?')
                time.sleep(2.5)
                content1=myCommand()
                webbrowser.open('https://www.google.co.in/search?q='+str(content1))
                time.sleep(3.5)
                time.sleep(2.5)
                pyautogui.click(380,378)
        elif content==('take picture camera'):
                time.sleep(2.5)
                pyautogui.click(91,767)
                time.sleep(1.5)
                #for char in (content[:12:-1]):
                    #time.sleep(3)
                    #pyautogui.click(91,767)
                keyboard.press('C')
                keyboard.release('C')
                keyboard.press('A')
                keyboard.release('A')
                keyboard.press('M')
                keyboard.release('M')
                time.sleep(1)
                keyboard.press(Key.enter)
                keyboard.release(Key.enter)
                time.sleep(5)
                pyautogui.click(1321,378)
        elif content==('GK'):
                time.sleep(2.5)
                #pyautogui.click(517,750)
                #time.sleep(2)
                pyautogui.click(365,749)
                talkToMe('What do you want to know?')
                content2=myCommand()
                
        else:
                webbrowser.open('https://www.google.co.in/search?q='+str(content))
talkToMe('I am ready for your command')
assistant(myCommand())

