import sys
import random
import math
import pygame
import pygame.gfxdraw
from pygame.locals import *
pygame.init()

CLOCK = pygame.time.Clock()
''' DISPLAY SETUP -------------------------------------------------------------------------------- DISPLAY SETUP '''
DISPLAY_WIDTH = 1280
DISPLAY_HEIGHT = 720
DW_HALF = DISPLAY_WIDTH / 2
DH_HALF = DISPLAY_HEIGHT / 2
DISPLAY_AREA = DISPLAY_WIDTH * DISPLAY_HEIGHT
DS = pygame.display.set_mode((DISPLAY_WIDTH, DISPLAY_HEIGHT))
''' LOAD IMAGES ---------------------------------------------------------------------------------- LOAD IMAGES '''

CATS = list([pygame.image.load('catwalk_{0}.png'.format(i)) for i in range(1, 13)])
RECT = CATS[0].get_rect()
''' SETUP VARIABLES ------------------------------------------------------------------------------ SETUP VARIABLES '''
FRAME_COUNT = 12
''' FUNCTIONS AND CLASSES ------------------------------------------------------------------------ FUNCTIONS AND CLASSES '''
def event_handler():
	for event in pygame.event.get():
		if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
			pygame.quit()
			sys.exit()

''' SETUP SOME MORE VARIABLES -------------------------------------------------------------------- SETUP SOME MORE VARIABLES '''
# the starting frame of the animation
frame = 0

''' MAIN LOOP ------------------------------------------------------------------------------------ MAIN LOOP '''
while True:
	event_handler()

	# draw the portion of the cat image that relates to the frame we wish to display
	# remember FRAMES is a list of offsets in the form of a rectangle (x, y, width, height)
	DS.blit(CATS[frame], (DW_HALF - RECT.center[0], DH_HALF - RECT.center[1]))
	
	# increment the frame count and if the frame count is greater than FRAME_COUNT - 1 then reset to 0
	frame += 1
	if frame > FRAME_COUNT - 1:
		frame = 0
		
	pygame.display.update()
	
	CLOCK.tick(5) # *** NOT COVERED IN THE VIDEO ***
	
	# because the cat in the animation is black, the background colour needs to be something other
	# than that. In this case it's grey
	DS.fill((128, 128, 128))
