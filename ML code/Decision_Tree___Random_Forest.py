
#Desicion Tree Algorithm


my_data = pandas.read_csv('skulls.csv')
featureNames = list(my_data.columns.values)[2:6]
# Remove the column containing the target name since it doesn't contain numeric values.
# axis=1 means we are removing columns instead of rows.
X = my_data.drop(my_data.columns[[0,1]], axis=1).values
X[0:5]
targetNames = my_data["epoch"].unique().tolist()
targetNames
y = my_data["epoch"]
y[0:5]
from sklearn.cross_validation import train_test_split
X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)
print X_trainset.shape 
print y_trainset.shape
print X_testset.shape 
print y_testset.shape
skullsTree = DecisionTreeClassifier(criterion="entropy")
skullsTree.fit(X_trainset,y_trainset)
predTree = skullsTree.predict(X_testset)
print(predTree)
print(y_testset)
from sklearn import metrics
import matplotlib.pyplot as plt
print("DecisionTrees's Accuracy: "), metrics.accuracy_score(y_testset, predTree)
from sklearn.externals.six import StringIO

#Random Forest Classifier

import pydotplus
import matplotlib.image as mpimg
from sklearn import tree
%matplotlib inline
dot_data = StringIO()
filename = "skulltree.png"
out=tree.export_graphviz(skullsTree,feature_names=featureNames, out_file=dot_data, class_names= np.unique(y_trainset), filled=True,  special_characters=True,rotate=False)  
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  
graph.write_png(filename)
img = mpimg.imread(filename)
plt.figure(figsize=(100, 200))
plt.imshow(img,interpolation='nearest')

from sklearn.ensemble import RandomForestClassifier
import numpy as np 
from sklearn.cross_validation import train_test_split
import pandas 
my_data = pandas.read_csv("https://vincentarelbundock.github.io/Rdatasets/csv/HSAUR/skulls.csv", delimiter=",")
X = my_data.drop(my_data.columns[[0,1]], axis=1).values
y = my_data["epoch"]
skullsForest=RandomForestClassifier(n_estimators=10,criterion='entropy')
skullsForest=RandomForestClassifier(n_estimators=10,criterion='entropy')
predForest=skullsForest.predict(X_testset)
print predForest
print y_testset
from sklearn import metrics
print("RandomForests's Accuracy: "), metrics.accuracy_score(y_testset, predForest)
print(skullsForest.estimators_)

