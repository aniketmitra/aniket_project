import numpy as np
import pandas
from sklearn.neighbors import KNeighborsClassifier
my_data=pandas.read_csv('skulls.csv',delimiter=',')
print(my_data)
print(type(my_data))
print(my_data.columns)
print(my_data.values)
print(my_data.shape)
def removeColumns(pandasArray,*column):
    return pandasArray.drop(pandasArray.columns[[column]],axis=1).values
new_data=removeColumns(my_data,0,1)
print(new_data)
def targetAndtargetNames(numpyArray,targetColumnIndex):
    target_dict=dict()
    target=list()
    target_names=list()
    count=-1
    for i in range (len(my_data.values)):
        if my_data.values[i][targetColumnIndex] not in target_dict:
            count+=1
            target_dict[my_data.values[i][targetColumnIndex]]=count
        target.append(target_dict[my_data.values[i][targetColumnIndex]])
    for targetName in sorted(target_dict,key=target_dict.get):
        target_names.append(targetName)
    return np.asarray(target),target_names
target,target_names=targetAndtargetNames(my_data,1)
print(target)
print(target_names)
X = new_data
y = target
neigh = KNeighborsClassifier(n_neighbors=1)
neigh.fit(X,y)
print('Prediction: ', neigh.predict(new_data[10]))
print('Actual:', y[10])
